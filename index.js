var prevSelectedCard = null
var overlayToggle = false

var lastPrev = null, lastNext = null

var CARD = 'card'
var GLOBAL_OVERLAY = 'global-overlay'
var SELECTED = 'selected', 
    PREV = 'prev', 
    NEXT = 'next'

var EVENT_CLICK_ACTION = 'click',
    EVENT_KEYDOWN_ACTION = 'keydown'
var NONE = 'none',
    BLOCK = 'block'
var HIDE_LEFT = 'hideLeft', 
    HIDE_RIGHT = 'hideRight', 
    PREV_LEFT_SECOND = 'prevLeftSecond', 
    NEXT_RIGHT_SECOND = 'nextRightSecond'
var isFlipped = 'is-flipped', isFlippedDetect = 'is-flipped-detect'

initFlip()
bindNextPrevActionButton()
bindNextPrevActionKeyboard()

function initFlip() {
    cardSwipeEventBinder()
}

function updateFlip() {
    removeAllEventListenersFromElement(prevSelectedCard)
    cardSwipeEventBinder()
}

// Iterate over cards and binding event listener
function cardSwipeEventBinder() {
    var selectedEl = classElements(SELECTED)[0] // Current selected element
    var cards = selectedEl.getElementsByClassName(CARD) // Array of cards
    for(var i=0;i<cards.length;i++) {
        var card = cards[i]
        bindClickEvent(card)
    }
}

// flipping of card when clicked to the card
function bindClickEvent(card) {
    prevSelectedCard = card
    card.addEventListener(EVENT_CLICK_ACTION, function() {
        var displayStyle = classElements(GLOBAL_OVERLAY)[0].style.display
        var selected = classElements(SELECTED)[0]
        card.classList.toggle(isFlipped) // animation class for card when flipping
        selected.classList.toggle(isFlippedDetect) // class to change size of card when flipped
        switchGlobalOverlay(displayStyle)
        overlayToggle = !overlayToggle
    }, true)
}

function switchGlobalOverlay(displayStyle) {
    if (displayStyle === BLOCK) {
        classElements(GLOBAL_OVERLAY)[0].style.display = NONE
    } else {
        classElements(GLOBAL_OVERLAY)[0].style.display = BLOCK
    }
}

// button click actions
function bindNextPrevActionButton() {
    var prevButton = idElement(PREV)
    var nextButton = idElement(NEXT)
    prevButton.addEventListener(EVENT_CLICK_ACTION, function(e) {
        moveToSelected(PREV)
        updateFlip()
    })
    nextButton.addEventListener(EVENT_CLICK_ACTION, function(e) {
        moveToSelected(NEXT)
        updateFlip()
    })
}

// keyboard click actions
function bindNextPrevActionKeyboard() {
    document.addEventListener(EVENT_KEYDOWN_ACTION, function(e) {
        if (overlayToggle === false) {
            switch(e.which) {
                case 37:
                    moveToSelected(PREV)
                    updateFlip()
                    break
                case 39:
                    moveToSelected(NEXT)
                    updateFlip()
                    break
                default: return
            }
            e.preventDefault()
        }
    })
}

// switch cards 
function moveToSelected(element) {
    if (element === NEXT) {
      var selected = classElements(SELECTED)[0].nextElementSibling
    } else if (element === PREV) {
      var selected = classElements(SELECTED)[0].previousElementSibling
    } else {
      var selected = element
    }
    redistributeClassNames(selected)
}

function redistributeClassNames(selected) {
    if (selected !== null) {
        var prev = selected.previousElementSibling
        if (prev === null) { prev = lastPrev }
        
        var next = selected.nextElementSibling
        if (next === null) { next = lastNext }
        
        var prevSecond = prev.previousElementSibling
        var nextSecond = next.nextElementSibling
        
        prev.className = PREV
        lastPrev = prev
        next.className = NEXT
        lastNext = next

        if (nextSecond !== null) { nextSecond.className = NEXT_RIGHT_SECOND } 
        if (prevSecond !== null) { prevSecond.className = PREV_LEFT_SECOND }
        
        for(var i=0;i<getNextSiblings(nextSecond, filter).length;i++) {
            var item = getNextSiblings(nextSecond, filter)[i]
            item.className = HIDE_RIGHT
        }
        
        for(var j=0;j<getPreviousSiblings(prevSecond, filter).length;j++) {
            var item = getPreviousSiblings(prevSecond, filter)[j]
            item.className = HIDE_LEFT
        }
        selected.className = SELECTED
    }
}

// support functions------------------------------------------------

// Get element and remove all event listener from element
function removeAllEventListenersFromElement(element) {
    let clone = element.cloneNode()
    while (element.firstChild) {
        clone.appendChild(element.lastChild)
    }
    element.parentNode.replaceChild(clone, element)
}

// return array of elements of next siblings
function getNextSiblings(el, filter) {
    if (el !== null) {
        var siblings = [];
        while (el= el.nextSibling) { if (!filter || filter(el)) siblings.push(el) }
        return siblings;
    } else { return [] }
}

// the same as getNextSiblings method
function getPreviousSiblings(el, filter) {
    if (el !== null) {
        var siblings = []
        while (el = el.previousSibling) { if (!filter || filter(el)) siblings.push(el) }
        return siblings
    } else { return [] }
}

// return only div elements and ignore another elements
function filter(el) {
    return el.nodeName.toLowerCase() === 'div'
}

function classElements(className) {
    return document.getElementsByClassName(className)
}

function idElement(idName) {
    return document.getElementById(idName)
}